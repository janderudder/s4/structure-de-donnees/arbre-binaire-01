package src;


public abstract class Arbre
{
	public abstract String getRacine();
	public abstract Arbre getAg();
	public abstract Arbre getAd();

	public abstract void setRacine(String s);
	public abstract void setAg(Arbre Ag);
	public abstract void setAd(Arbre Ad);

	public abstract boolean estVide();

	public abstract void afficherGRD();

	public abstract int nbFeuilles();

	public abstract boolean trouver (String element);

	public abstract String lePlusAGauche();
	public abstract String lePlusADroite();

	public abstract Arbre supprimer(String valeur);
}



class ArbreVide extends Arbre
{
	ArbreVide ()
	{
	}

	@Override
	public String getRacine() { return null; }
	@Override
	public Arbre getAg() { return this; }
	@Override
	public Arbre getAd() { return this; }

	@Override
	public void setRacine(String s) {  }
	@Override
	public void setAg(Arbre Ag) {  }
	@Override
	public void setAd(Arbre Ad) {  }

	@Override
	public boolean estVide()
	{
		return true;
	}

	@Override
	public void afficherGRD() {}

	@Override
	public int nbFeuilles() { return 0; }

	@Override
	public boolean trouver (String element) {
		return false;
	}

	@Override
	public String lePlusAGauche() {
		return null;
	}

	@Override
	public String lePlusADroite() {
		return null;
	}

	@Override
	public Arbre supprimer(String valeur) {
		return null;
	}

}



class ArbreCons extends Arbre
{
	private String racine;
	private Arbre Ag;
	private Arbre Ad;

	ArbreCons(String val, Arbre Ag, Arbre Ad)
	{
		this.racine = val;
		this.Ag = Ag;
		this.Ad = Ad;
	}

	ArbreCons( String val)
	{
		this.racine = val; this.Ag = new ArbreVide(); this.Ad = new ArbreVide();
	}


	@Override
	public String getRacine() { return this.racine; }
	@Override
	public Arbre getAg() { return this.Ag; }
	@Override
	public Arbre getAd() { return this.Ad; }

	@Override
	public void setRacine(String s) { this.racine = s; }
	@Override
	public void setAg(Arbre Ag) { this.Ag = Ag; }
	@Override
	public void setAd(Arbre Ad) { this.Ad = Ad; }


	@Override
	public boolean estVide()
	{
		return false;
	}

	public boolean estFeuille()
	{
		return this.getAg().estVide() && this.getAd().estVide();
	}

	@Override
	public void afficherGRD()
	{
		this.Ag.afficherGRD();
		System.out.println(this.racine);
		this.Ad.afficherGRD();
	}

	@Override
	public int nbFeuilles()
	{
		if (this.Ag.estVide() && this.Ad.estVide()) {
			return 1;
		} else {
			return this.Ag.nbFeuilles() + this.Ad.nbFeuilles();
		}
	}

	@Override
	public boolean trouver (String element) {
		return this.racine.equals(element)
			? true
			: this.Ag.trouver(element) || this.Ad.trouver(element);
	}


	@Override
	public String lePlusAGauche() {
		return !this.Ag.estVide()
			? this.Ag.lePlusAGauche()
			: this.racine;
	}

	@Override
	public String lePlusADroite() {
		return !this.Ad.estVide()
			? this.Ad.lePlusADroite()
			: this.racine;
	}

	/**
	 * Lorsqu'on supprime la racine, mettre le noeud le plus à gauche de
	 * l'arbre à la place.
	 */
	@Override
	public Arbre supprimer(String valeur)
	{
		if (this.racine.equals(valeur)) {
			String pg = this.lePlusAGauche();
			if (pg != null) {
				this.Ag.supprimer(pg);
				this.racine = pg;
			} else {
				this.racine = null;
			}
		}
		return this;
	}
}
