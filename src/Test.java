package src;


public class Test
{
    public static void main( String [] arg)
    {
        Arbre a1 = new ArbreCons("UN");     // Feuille
        Arbre a2 = new ArbreCons("DEUX");   // Feuille

        Arbre a3 = new ArbreCons( "TROIS", a1, a2);
        Arbre a4 = new ArbreCons( "QUATRE", a3, new ArbreCons("CINQ"));

        a4.afficherGRD();

        System.out.println(a4.nbFeuilles());

        System.out.println("a4.trouver(\"SIX\"): "+a4.trouver("SIX"));
        System.out.println("a4.trouver(\"UN\"): "+a4.trouver("UN"));
        System.out.println("a4.trouver(\"DEUX\"): "+a4.trouver("DEUX"));
        System.out.println("a4.trouver(\"TROIS\"): "+a4.trouver("TROIS"));
        System.out.println("a4.trouver(\"QUATRE\"): "+a4.trouver("QUATRE"));
        System.out.println("a4.trouver(\"CINQ\"): "+a4.trouver("CINQ"));

        System.out.println("a4.lePlusAGauche(): "+a4.lePlusAGauche());
        System.out.println("a4.lePlusADroite(): "+a4.lePlusADroite());

        System.out.println("Suppression CINQ");
        System.out.println("-------------------");
        a4 = a4.supprimer("CINQ");
        a4.afficherGRD();
    }
}
